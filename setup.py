from setuptools import setup, find_namespace_packages

setup(
    name='hardware.flash_spi_der',
    version='0.1.0',
    description='SPI-flash communication',
    author='Invuls',
    packages=find_namespace_packages(),
    package_data={'isf.hardware.flash_spi_der.resources': ['*', '**/*']},
    zip_safe=False
)

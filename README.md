# flash_spi_der 

Original utility: https://github.com/nfd/spi-flash-programmer

Module for SPI flash communication


Connect tutorial:

| Flash pin num | Flash pin name | Arduino pin |
|---------------|----------------|-------------|
| 1             | SS             | 10          |
| 2             | MISO           | 12          |
| 3             | WP             | +3.3V       |
| 4             | GND            | GND         |
| 5             | MOSI           | 11          |
| 6             | SCK(CLK)       | 13          |
| 7             | HOLD           | +3.3V       |
| 8             | VDD            | +3.3V       |

![Image of SPI SOP8 chip pinout](https://sebastien.andrivet.com/media/original_images/Connections_Bus_Pirate.png)


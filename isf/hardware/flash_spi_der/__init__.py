from isf.core import logger
import isf.hardware.avrloader
from isf.hardware.flash_spi_der.resources.spi_flash_programmer.spi_flash_programmer_client import \
    SerialProgrammer
import time
import os


class SPIder:
    com_port = '/dev/ttyUSB0'
    need_to_program = True

    def __init__(self, com_port='/dev/ttyUSB0', need_to_program=True):
        # arduino or other device com port
        self.com_port = com_port
        self.need_to_program = need_to_program

        logger.info(
            'Original tool: https://github.com/nfd/spi-flash-programmer')
        logger.info('Check "SPI flash->Arduino" connect pinout at https://gitlab.com/invuls/iot-projects/iotsecfuzz-modules/hardware/flash_spi_der')

        if self.need_to_program:
            self.prepare_uno()

    def __del__(self):
        pass

    def prepare_uno(self):
        wait_time = 10
        logger.info('Programming arduino for dumping SPI flash memory.')
        uno_obj = isf.hardware.avrloader.AVRLoader()
        ino_path = os.path.dirname(os.path.realpath(
            __file__)) + '\\resources\\spi_flash_programmer\\spi_flash_programmer.ino'
        logger.debug(ino_path)
        uno_obj.ino_uploader(
            ino_path=ino_path,
            com_port=self.com_port,
            tmp_hex=".\\isf.tmp"
        )
        del uno_obj
        logger.info('Wait for {} sec, please!'.format(wait_time))
        time.sleep(wait_time)
        self.need_to_program = False

    def dump_uno(self, dump_path="1.bin", offset=0, length=4096):
        spi_object = SerialProgrammer(port=self.com_port, baud_rate=115200,
                                      debug='off')
        logger.info(str(
            spi_object.readToFile(filename=dump_path, flash_offset=offset,
                                  length=length)))

    def erase_uno(self, offset=0, length=4096):
        spi_object = SerialProgrammer(port=self.com_port, baud_rate=115200,
                                      debug='off')
        logger.info(str(
            spi_object.erase(flash_offset=offset, length=length)))

    def verify_uno(self, check_path="check.bin", flash_offset=0, file_offset=0, length=4096):
        spi_object = SerialProgrammer(port=self.com_port, baud_rate=115200,
                                      debug='off')
        status = spi_object.verifyWithFile(filename=check_path,
                                                  flash_offset=flash_offset,
                                                  file_offset=file_offset,
                                                  length=length)
        return status

    def protect_uno(self):
        spi_object = SerialProgrammer(port=self.com_port, baud_rate=115200,
                                      debug='off')
        logger.info(str(spi_object.set_write_protection(True)))
        return

    def unprotect_uno(self):
        spi_object = SerialProgrammer(port=self.com_port, baud_rate=115200,
                                      debug='off')
        logger.info(str(spi_object.set_write_protection(False)))

    def isprotect_uno(self):
        spi_object = SerialProgrammer(port=self.com_port, baud_rate=115200,
                                      debug='off')
        status = spi_object.check_write_protection()
        return status

    def status_register_uno(self):
        spi_object = SerialProgrammer(port=self.com_port, baud_rate=115200,
                                      debug='off')
        status = spi_object.read_status_register()
        return status

    def id_register_uno(self):
        spi_object = SerialProgrammer(port=self.com_port, baud_rate=115200,
                                      debug='off')
        status = spi_object.read_id_register()
        return status



